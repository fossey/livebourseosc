import requests
import json
import time
import re
import argparse
import urllib3
from pythonosc import udp_client

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

STOCK_EXCHANGES = ['CAC40', 'Nasdaq', 'Nikkei', 'Petrole', 'OR']


def _format_value(value):
    return re.sub(r"\s+", "", str(value), re.UNICODE).replace(',', '.')


def _get_stock_exchange_data() -> dict:
    response = requests.get(
        url="https://lesechos-bourse-fo-cdn.wlb.aw.atos.net/streaming/cours/getHeaderBourse",
        verify=False,
        headers={'Content-Type': "application/json"}
    )
    return json.loads(response.text)


def _extract_data(data: dict) -> dict:
    output = ''
    osc = dict()
    for header_name in STOCK_EXCHANGES:
        json_element = data.get('header' + header_name).get('indiceheader')
        variation = _format_value(json_element.get('header_bourse_stream_variation'))
        share_value = _format_value(json_element.get('header_bourse_stream_valorisation_gauche'))

        output += f"{share_value} {variation} "

        osc.update({
            header_name: {
                "share_value": float(share_value),
                "variation": float(variation)
            }
        })

    return osc


def _values_changed(latest_values: dict, saved_values: dict) -> bool:
    if not saved_values:
        return True

    for exchanges in latest_values:
        if latest_values.get(exchanges).get("share_value") != saved_values.get(exchanges).get("share_value"):
            return True
        if latest_values.get(exchanges).get("variation") != saved_values.get(exchanges).get("variation"):
            return True
    return False


def _send_values(client, values_to_send: dict):
    values_formatted = []
    for h in STOCK_EXCHANGES:
        values_formatted.append(values_to_send.get(h).get("share_value"))
        values_formatted.append(values_to_send.get(h).get("variation"))

    client.send_message("/livebourse", values_formatted)

    print(f'SENT > {values_formatted}')


if __name__ == '__main__':

    usage = " python stock_exchanges_monitoring.py -d 5 -n 100\n\n"
    usage += "\tEach 5 seconds program does an API call until he does 100\n"
    usage += "\tIf -n argument is -1, programs will loop infinitely"

    description = "\tMonitored data will be sent via OSC like :\n"
    description += "\t5548.45 -0.31 7424.43 -0.02 22437.01 -1.11 78.732 -1.20 1304.330 0.85\n"
    description += "\t----CAC40---- ---Nasdaq---- ----Nikkei---- ---Petrole-- ------OR-----\n"

    parser = argparse.ArgumentParser(usage=usage, description=description,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-d", "--delay", help="delay in second between each API call", default=2, type=int)
    parser.add_argument("-n", "--call-numbers", help="does N API calls before program stopping", default=-1, type=int)
    parser.add_argument("--on-change", help="will send data via OSC only if they changed with last time", default=False,
                        action='store_true')
    parser.add_argument("-v", "--verbose", help="active verbose mode", default=False, action='store_true')
    args = parser.parse_args()

    ip, port = '127.0.0.1', 9000

    print("\n\t####################################################################")
    print(f"\t##############       Stock Exchanges Monitoring      ############### ")
    print(f"\t############## > data sent via OSC on {ip}:{port} ############### ")
    print("\t####################################################################\n")

    if args.verbose:
        print("\t####################################################################")
        print(f"\t#####################      configuration      ###################### ")
        print("\t####################################################################")
        print(f"\t> delay = {args.delay} seconds" +
              f"\t> max API calls = {args.call_numbers if args.call_numbers != -1 else 'Infinite'}")
        print(f"\t> on change = {args.on_change}" +
              f"\t> verbose = {args.verbose}\n")

    udp_client = udp_client.SimpleUDPClient(ip, port)

    max_api_call = args.call_numbers
    api_call = 0
    latest_data_sent = {}

    while api_call < max_api_call or max_api_call == -1:
        latest_data_received = _extract_data(_get_stock_exchange_data())

        if args.on_change:
            if _values_changed(latest_data_received, latest_data_sent):
                _send_values(udp_client, latest_data_received)
                latest_data_sent = latest_data_received
        else:
            _send_values(udp_client, latest_data_received)

        time.sleep(args.delay)

        api_call += 1
