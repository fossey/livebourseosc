# Monitoring stock exchanges ( transfer data via OSC )

### Set up

````shell
$ sudo pip3 install pipenv
$ pipenv sync
````

### How it works

````shell
$ pipenv shell 
$ python stock_exchanges_monitoring.py --help
````

then read ~~the fucking~~ manual

### Data flow example

```json
{
  "headerCAC40": {
    "indiceheader": {
      "code": "FR0003500008",
      "place": "XPAR",
      "codif": "ISIN",
      "name": "CAC 40",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-cac-40,xpar,px1,fr0003500008,isin.html",
      "header_bourse_stream_variation": "0,52",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "5 594,63",
      "header_bourse_stream_valorisation_enc": "5 594,63$"
    }
  },
  "headerPetrole": {
    "indiceheader": {
      "code": "C:EBROUSDBR.SP",
      "place": "WCOMO",
      "codif": "ISO",
      "name": "Pétrole Brent",
      "urlFiche": "https://investir.lesechos.fr/cours/matiere-premiere-petrole-brent,wcomo,brent,cebrousdbrsp,iso.html",
      "header_bourse_stream_variation": "-0,63",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "79,192",
      "header_bourse_stream_valorisation_enc": "79,192$"
    }
  },
  "headerOR": {
    "indiceheader": {
      "code": "C:SXAUUSDOZ.SP",
      "place": "WCOMO",
      "codif": "ISO",
      "name": "Once Or",
      "urlFiche": "https://investir.lesechos.fr/cours/matiere-premiere-once-or,wcomo,gold,csxauusdozsp,iso.html",
      "header_bourse_stream_variation": "0,27",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "1 296,856",
      "header_bourse_stream_valorisation_enc": "1 296,856$"
    }
  },
  "headerEURUS": {
    "indiceheader": {
      "code": "EURUSD",
      "place": "WFORX",
      "codif": "ISO",
      "name": "EUR/USD",
      "urlFiche": "https://investir.lesechos.fr/cours/devise-eur-usd,wforx,eurusd,eurusd,iso.html",
      "header_bourse_stream_variation": "0,36",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "1,1738",
      "header_bourse_stream_valorisation_enc": "1,1738$"
    }
  },
  "headerDowJones": {
    "indiceheader": {
      "code": "DJI",
      "place": "XNYS",
      "codif": "TICK",
      "name": "Dow Jones IA",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-dow-jones-ia,xnys,us2605661048,dji,tick.html",
      "header_bourse_stream_variation": "0,21",
      "state": "(c)",
      "header_bourse_stream_valorisation_gauche": "24 886,81",
      "header_bourse_stream_valorisation_enc": "24 886,81$"
    }
  },
  "headerEuroStoxx": {
    "indiceheader": {
      "code": "EU0009658145",
      "place": "WDSTX",
      "codif": "ISIN",
      "name": "Euro Stoxx 50",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-euro-stoxx-50,wdstx,eu0009658145,eu0009658145,isin.html",
      "header_bourse_stream_variation": "0,31",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "3 552,86",
      "header_bourse_stream_valorisation_enc": "3 552,86$"
    }
  },
  "headerNasdaq": {
    "indiceheader": {
      "code": "COMP",
      "place": "XNAS",
      "codif": "TICK",
      "name": "Nasdaq Comp.",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-nasdaq-comp,xnas,xc0009694271,comp,tick.html",
      "header_bourse_stream_variation": "0,64",
      "state": "(c)",
      "header_bourse_stream_valorisation_gauche": "7 425,96",
      "header_bourse_stream_valorisation_enc": "7 425,96$"
    }
  },
  "headerI10GV": {
    "indiceheader": {
      "code": "FR0011630474",
      "place": "XPAR",
      "codif": "ISIN",
      "name": "INVESTIR 10 GRANDES VALEURS",
      "urlFiche": "https://investir.lesechos.fr/cours/certificat-investir-10-grandes-valeurs,xpar,i10gs,fr0011630474,isin.html",
      "header_bourse_stream_variation": "-0,12",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "149,05",
      "header_bourse_stream_valorisation_enc": "149,05$"
    }
  },
  "headerENTPEAPME": {
    "indiceheader": {
      "code": "FR0012246023",
      "place": "XPAR",
      "codif": "ISIN",
      "name": "ENTERNEXT PEA PME 150",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-enternext-pea-pme-150,xpar,enpme,fr0012246023,isin.html",
      "header_bourse_stream_variation": "0,01",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "2 113,53",
      "header_bourse_stream_valorisation_enc": "2 113,53$"
    }
  },
  "headerNikkei": {
    "indiceheader": {
      "code": "I:N225",
      "place": "WINDX",
      "codif": "ISO",
      "name": "Nikkei 225",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-nikkei-225,windx,xc0009692440,in225,iso.html",
      "header_bourse_stream_variation": "-1,11",
      "state": "(c)",
      "header_bourse_stream_valorisation_gauche": "22 437,01",
      "header_bourse_stream_valorisation_enc": "22 437,01$"
    }
  },
  "headerDAX": {
    "indiceheader": {
      "code": "DE0008469008",
      "place": "XETR",
      "codif": "ISIN",
      "name": "DAX Xetra",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-dax-xetra,xetr,dax,de0008469008,isin.html",
      "header_bourse_stream_variation": "0,11",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "12 991,48",
      "header_bourse_stream_valorisation_enc": "12 991,48$"
    }
  },
  "headerPalmaresSRDHausse": {
    "items": [
      {
        "code": "NL0006294274",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "EURONEXT",
        "urlFiche": "https://investir.lesechos.fr/cours/action-euronext,xpar,enx,nl0006294274,isin.html",
        "header_bourse_stream_palmares_variation": "3,00",
        "volumeHeader": "5 490",
        "header_bourse_stream_palmares_valorisation_gauche": "53,15",
        "iriForGraph": "NL0006294274*ISIN*XPAR"
      },
      {
        "code": "FR0000054470",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "UBISOFT ENTERTAINMENT",
        "urlFiche": "https://investir.lesechos.fr/cours/action-ubisoft-entertainment,xpar,ubi,fr0000054470,isin.html",
        "header_bourse_stream_palmares_variation": "2,73",
        "volumeHeader": "12 410",
        "header_bourse_stream_palmares_valorisation_gauche": "88,86",
        "iriForGraph": "FR0000054470*ISIN*XPAR"
      },
      {
        "code": "FR0000034639",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "ALTRAN TECHNOLOGIES",
        "urlFiche": "https://investir.lesechos.fr/cours/action-altran-technologies,xpar,alt,fr0000034639,isin.html",
        "header_bourse_stream_palmares_variation": "2,38",
        "volumeHeader": "1 482",
        "header_bourse_stream_palmares_valorisation_gauche": "13,33",
        "iriForGraph": "FR0000034639*ISIN*XPAR"
      },
      {
        "code": "NL0000226223",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "STMICROELECTRONICS",
        "urlFiche": "https://investir.lesechos.fr/cours/action-stmicroelectronics,xpar,stm,nl0000226223,isin.html",
        "header_bourse_stream_palmares_variation": "2,34",
        "volumeHeader": "10 729",
        "header_bourse_stream_palmares_valorisation_gauche": "20,33",
        "iriForGraph": "NL0000226223*ISIN*XPAR"
      },
      {
        "code": "FR0013227113",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "SOITEC",
        "urlFiche": "https://investir.lesechos.fr/cours/action-soitec,xpar,soi,fr0013227113,isin.html",
        "header_bourse_stream_palmares_variation": "2,31",
        "volumeHeader": "1 420",
        "header_bourse_stream_palmares_valorisation_gauche": "77,65",
        "iriForGraph": "FR0013227113*ISIN*XPAR"
      }
    ]
  },
  "headerPalmaresSRDBaisse": {
    "items": [
      {
        "code": "US3696041033",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "GENERAL ELECTRIC",
        "urlFiche": "https://investir.lesechos.fr/cours/action-general-electric,xpar,gne,us3696041033,isin.html",
        "header_bourse_stream_palmares_variation": "-5,32",
        "volumeHeader": "214",
        "header_bourse_stream_palmares_valorisation_gauche": "12,10",
        "iriForGraph": "US3696041033*ISIN*XPAR"
      },
      {
        "code": "FR0010918292",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "TECHNICOLOR",
        "urlFiche": "https://investir.lesechos.fr/cours/action-technicolor,xpar,tch,fr0010918292,isin.html",
        "header_bourse_stream_palmares_variation": "-2,14",
        "volumeHeader": "2 160",
        "header_bourse_stream_palmares_valorisation_gauche": "1,56",
        "iriForGraph": "FR0010918292*ISIN*XPAR"
      },
      {
        "code": "FR0000120503",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "BOUYGUES",
        "urlFiche": "https://investir.lesechos.fr/cours/action-bouygues,xpar,en,fr0000120503,isin.html",
        "header_bourse_stream_palmares_variation": "-1,51",
        "volumeHeader": "9 021",
        "header_bourse_stream_palmares_valorisation_gauche": "41,14",
        "iriForGraph": "FR0000120503*ISIN*XPAR"
      },
      {
        "code": "FR0000121501",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "PEUGEOT",
        "urlFiche": "https://investir.lesechos.fr/cours/action-peugeot,xpar,ug,fr0000121501,isin.html",
        "header_bourse_stream_palmares_variation": "-1,40",
        "volumeHeader": "13 486",
        "header_bourse_stream_palmares_valorisation_gauche": "20,45",
        "iriForGraph": "FR0000121501*ISIN*XPAR"
      },
      {
        "code": "FR0000053225",
        "place": "XPAR",
        "codif": "ISIN",
        "name": "METROPOLE TV",
        "urlFiche": "https://investir.lesechos.fr/cours/action-metropole-tv,xpar,mmt,fr0000053225,isin.html",
        "header_bourse_stream_palmares_variation": "-1,36",
        "volumeHeader": "1 048",
        "header_bourse_stream_palmares_valorisation_gauche": "18,82",
        "iriForGraph": "FR0000053225*ISIN*XPAR"
      }
    ]
  },
  "headerPEAPALATINE": {
    "indiceheader": {
      "code": "",
      "place": "",
      "codif": "",
      "name": "",
      "urlFiche": "https://investir.lesechos.fr/cours/",
      "header_bourse_stream_variation": "",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "",
      "header_bourse_stream_valorisation_enc": "$"
    }
  },
  "headerFTSE100": {
    "indiceheader": {
      "code": "UKX",
      "place": "XLON",
      "codif": "TICK",
      "name": "FTSE 100",
      "urlFiche": "https://investir.lesechos.fr/cours/indice-ftse-100,xlon,gb0001383545,ukx,tick.html",
      "header_bourse_stream_variation": "-0,07",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "7 783,06",
      "header_bourse_stream_valorisation_enc": "7 783,06$"
    }
  },
  "headerOAT10ANS": {
    "indiceheader": {
      "code": "B:BMFR.10Y",
      "place": "WRATE",
      "codif": "ISO",
      "name": "OAT 10 ans",
      "urlFiche": "https://investir.lesechos.fr/cours/rate-oat-10-ans,wrate,bbmfr10y,bbmfr10y,iso.html",
      "header_bourse_stream_variation": "-0,21",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "0,81",
      "header_bourse_stream_valorisation_enc": "0,81%"
    }
  },
  "headerTBOND10ANS": {
    "indiceheader": {
      "code": "B:BMUS.10Y",
      "place": "WRATE",
      "codif": "ISO",
      "name": "US Treas 2.8750% 15-May-2028",
      "urlFiche": "https://investir.lesechos.fr/cours/rate-us-treas-28750pc-15-may-2028,wrate,bbmus10y,bbmus10y,iso.html",
      "header_bourse_stream_variation": "0,58",
      "state": "",
      "header_bourse_stream_valorisation_gauche": "3,01",
      "header_bourse_stream_valorisation_enc": "3,01%"
    }
  }
}
```
