from pythonosc import dispatcher
from pythonosc import osc_server


def printing_handler(unused_addr, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10):
    print(f"RECEIVED > {v1:.3f} {v2:.2f}% {v3:.3f} {v4:.2f}% {v5:.3f} {v6:.2f}% {v7:.3f} {v8:.2f}% {v9:.3f} {v10:.2f}%")


ip, port = '127.0.0.1', 9000

dispatcher = dispatcher.Dispatcher()
dispatcher.map("/livebourse", printing_handler)

server = osc_server.ThreadingOSCUDPServer((ip, port), dispatcher)

print("\n\t####################################################################")
print(f"\t############## OSC Server - running on {ip}:{port} ############## ")
print("\t####################################################################\n")

server.serve_forever()
